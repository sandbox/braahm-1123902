Issue:
* Scheduled too many posts in the feature. Reschedule them in bulk so they get published faster

Functionality:
* Set the deadline for which all posts should be published
* Set the time intervall for each day for which the posts should be published
* Optionally: randomizer to let have the intervals a more natural look
* Optionally set the scheduled publishing interval for new posts (fe: every 3 days since the last post)

How:
* Calculate the time intervals for posts given
  - the deadline
  - the number of posts
  - time of day a post may be published
* Batch api: for each post:
  - update the publisch date
  
Extra's:
* Interface for rescheduling with javascript to make it more responsive
* Visualization of publishing posts with timeline


ToDo:
* disable form when no items are published
* make sure only publish on nodes are retrieved - currently it's also for unpublished nodes => not good


Batch

* call function that will call batch set => use this function to pass addiotional parameters
* batch_set('batchdefinition')
* $context automatically referenced
