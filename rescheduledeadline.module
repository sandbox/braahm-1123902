<?php

/**
 * @file
 * Main logic for rescheduledeadline module.
 */

/**
 * Implements hook_permission().
 */
function rescheduledeadline_permission() {
  return array(
    'reschedule content' => array(
      'title' => t('Reschedule content'),
      'description' => t('Allows users to reschedule all scheduled content'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function rescheduledeadline_menu() {
  $items = array();
  
  $items['admin/content/reschedule'] = array(
    'title' => 'Reschedule content',
    'desciption' => 'Reschedule the already scheduled content to be published before a given deadline',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('rescheduledeadline_admin_form'),
    'access arguments' => array('reschedule content'),
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/content/reschedule-info'] = array(
    'title' => 'Reschedule content info',
    'desciption' => 'Reschedule the already scheduled content to be published before a given deadline',
    'page callback' => 'rescheduledeadline_information',
    'access arguments' => array('reschedule content'),
    'type' => MENU_LOCAL_TASK,
  );
  
  return $items;
}


/**
 * Form to set scheduling.
 */
function rescheduledeadline_admin_form($form, &$form_state) {
  /*
  $output = scheduler_list();
  
  return $output;
  */
  $form = array();
  
  $form['rescheduledeadline'] = array(
    '#type' => 'fieldset',
    '#title' => t('Reschedule the existing scheduled posts'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  // If date popup module is enabled => add the popup widget
  $form['rescheduledeadline']['deadline'] = array(
    '#type' => 'textfield',
    '#title' => t('Deadline'),
    '#description' => t('Select the deadline for which you want to publish all posts'),
    '#required' => TRUE,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Reschedule'),
  );
  
  return $form;
  
}

/**
 * Form validation function.
 */
function rescheduledeadline_admin_form_validate($form, &$form_state) {

  //check if the value is in the correct format, otherwise set an error
  $deadline = $form_state['values']['deadline'];
  $deadline = _scheduler_strtotime($deadline);
  if($deadline == FALSE || $deadline == NULL)
    form_error($form['rescheduledeadline']['deadline'], t('You must insert the string as "Y-m-d H:i:s"'));
}


/**
 * Form submit function.
 */
function rescheduledeadline_admin_form_submit($form, &$form_state) {
  // get a the nids of all scheduled nodes
  // convert the deadline field to a unix timestamp
  // call the calcultate function
  // with the return value, do a batch api

  
  // get all nids of scheduled nodes
  // access the objects with $result->nid
  $results = db_select('scheduler', 's')->fields('s', array('nid'))->execute();
  foreach($results as $result) {
    $nids[] = $result->nid;
  }

  $deadline = $form_state['values']['deadline'];
  $deadline = _scheduler_strtotime($deadline);
  
  $nidsWithTimestamp = rescheduledeadline_reschedule($nids, $deadline);
  dsm($nidsWithTimestamp);
  
  /*
  // create the batch directly here
  $batch = array(
    'operations' => array(
      array('rescheduledeadline_reschedule_process', array($nidsWithTimestamp)),
      ),
    'finished' => 'rescheduledeadline_reschedule_finished',
    'title' => t('Rescheduling Nodes in Batch'),
    'init_message' => t('Starting rescheduling nodes.'),
    'progress_message' => t('Rescheduled @current out of @total.'),
    'error_message' => t('We have encountered an error while rescheduling nodes.'),
    //'file' => drupal_get_path('module', 'batch_example') . '/batch_example.inc',
  );
  batch_set($batch);
  */
  
  //////////
  $num_operations = count($nidsWithTimestamp);;
  drupal_set_message(t('Creating an array of @num operations', array('@num' => $num_operations)));

  $operations = array();
  // Set up an operations array with 1000 elements, each doing function
  // batch_example_op_1.
  // Each operation in the operations array means at least one new HTTP request,
  // running Drupal from scratch to accomplish the operation. If the operation
  // returns with $context['finished'] != TRUE, then it will be called again.
  // In this example, $context['finished'] is always TRUE.
  foreach($nidsWithTimestamp as $nid => $timestamp) {
    $operations[] = array('rescheduledeadline_reschedule_nodes', array($nid, $timestamp));  
  }
  $batch = array(
    'operations' => $operations,
    'finished' => 'rescheduledeadline_finished',
    'title' => t('Rescheduling Nodes in Batch'),
    'init_message' => t('Starting rescheduling nodes.'),
    'progress_message' => t('Rescheduled @current out of @total.'),
    'error_message' => t('We have encountered an error while rescheduling nodes.'),
  );
  
  $_SESSION['http_request_count'] = 0; // reset counter for debug information.
  
  batch_set($batch);
}


/**
 * Basic page.
 *
 * We want to test rescheduledeadline_reschedule function
 * by outputting the result on the screen.
 */
function rescheduledeadline_information() {

  $output = '';

  // Set the deadline two weeks from now
  $deadline = (time() + (60 * 60 * 24 * 14));

  $nids = array();
  $nids[] = 12;
  $nids[] = 14;
  
  $results = rescheduledeadline_reschedule($nids, $deadline);
  
  foreach($results as $key => $value) {
    $output .= 'The node id is ' . $key . ' and will be published on ' . date('Y-m-d G:i', $value) . '<br />';
  }
  
  $results = db_select('scheduler', 's')->fields('s', array('nid'))/*->where()*/->execute();
  foreach($results as $result) {
    dsm($result->nid);
  }
  dsm($results);
  
  return $output;
  
}

/**
 * Reschedules nodes given a deadline.
 *
 * @param
 *   array nids
 *   date deadline
 *   int interval_start
 *   int interval_duration
 *   date time_from
 *
 * @return
 *   array rescheduled: nids => unixtimestamp
 *
 * For the moment, it doens't take the time intervals into account
 */
function rescheduledeadline_reschedule($nids, $deadline, $interval_start = 0, $interval_duration = 24, $time_from = NULL) {
   $rescheduled = array();
  
  // if the time from is not provided, set it as the current time
  $time_from = (isset($time_from) ? $time_from : time());
  
  /*
  // We are not using the time intervals yet
  
  // Make sure interval_start is between 0 & 23
  if($interval_start > 24)
    $interval_start = 23;
  if($interval_start < 0)
    $interval_start = 0;
  
  // Make sure that the duration will be within the current day
  if(($interval_start + $interval_duration) > 24)
    $interval_duration = 24 - $interval_start;
  */
  
  
  
  // Calculate the diffentence in time
  $differenceInTime = $deadline - $time_from;
  $interval = $differenceInTime/count($nids);

  // Build the array with nid and publish timestamp
  $sequence = 1;
  foreach($nids as $nid) {
    $rescheduled[$nid] = (int) round($time_from + ($interval * $sequence));
    $sequence++;
  }
  
  return $rescheduled;
  
}


/**
 * Start updateing the nodes with the batch api.
 *
 * @param
 *   associative array $nidsWithTimestamp
 *     key = nid
 *     value = timestamp
 *
 * @return ?
 */
/*
function rescheduledeadline_reschedule_process($nidsWithTimestamp, &$context){
  // first, set some context variables && only do this once
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['max'] = count($nidsWithTimestamp);
  }
  
  // we will only update one note at a time
  $node = node_load($);
  

}*/


/**
 * Rescheduling nodes with the batch api.
 */
function rescheduledeadline_reschedule_nodes($nid, $timestamp, &$context) {
  
  // Just dump the timestamps in de scheduler table instead of doing node_load, node_save
  $updated = db_update('scheduler')
  ->fields(array(
    'publish_on' => $timestamp,
  ))
  ->condition('nid', $nid, '=')
  ->execute();
  /*
  $node = node_load($nid);
  $title = $node->title;
  $title .= ' up 1';
  $node->scheduler['publish_on'] = $timestamp;
  $node->title = $title;
  // it's not being saved !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  node_save($node);
    dsm($node);
  */
  // Store some result for post-processing in the finished callback.
  $context['results'][] = $node->nid . ' : ' . check_plain($node->title);

  // Optional message displayed under the progressbar.
  $context['message'] = t('Loading node "@title"', array('@title' => $node->title));
  
  dsm('We are processing');
  $_SESSION['http_request_count']++;
}


/**
 * Finsish the batch operation.
 */
function rescheduledeadline_finished($success, $results, $operations) {
  if ($success) {
    // Here we could do something meaningful with the results.
    // We just display the number of nodes we processed...
    drupal_set_message(t('We did it'));
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing @operation with arguments : @args', array('@operation' => $error_operation[0], '@args' => print_r($error_operation[0], TRUE))));
  }
}